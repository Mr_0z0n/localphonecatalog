package com.mrozon.localphonecatalog.backuprestore;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.importfile.ImportFilePresenter;
import com.mrozon.localphonecatalog.importfile.ImportFileService;
import com.mrozon.localphonecatalog.importfile.ImportFileView;

import java.io.File;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BackupRestoreFragment extends MvpAppCompatFragment
        implements BackupRestoreView, View.OnClickListener {

    public static final String TAG = BackupRestoreFragment.class.getSimpleName();

    @BindView(R.id.buttonSaveBackup)
    Button buttonSaveBackup;

    @BindView(R.id.buttonRestoreBackup)
    Button buttonRestoreBackup;

    @BindView(R.id.buttonSaveBackupPhone)
    Button buttonSaveBackupPhone;

    @BindView(R.id.buttonRestoreBackupPhone)
    Button buttonRestoreBackupPhone;

    @BindView(R.id.progressBarBackup)
    ProgressBar progressBarBackup;

    @InjectPresenter
    BackupRestorePresenter presenter;

    public static BackupRestoreFragment newInstance() {
        Bundle args = new Bundle();
        BackupRestoreFragment fragment = new BackupRestoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void setTitle(int res){
        if(getActivity()!=null)
            getActivity().setTitle(res);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backup_restore, container, false);
        ButterKnife.bind(this, view);

        presenter.setContext(getContext());

        setTitle(R.string.backup_restore);

        buttonSaveBackup.setOnClickListener(this);
        buttonRestoreBackup.setOnClickListener(this);
        buttonSaveBackupPhone.setOnClickListener(this);
        buttonRestoreBackupPhone.setOnClickListener(this);


        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void showProgress() {
        progressBarBackup.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarBackup.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }

    @Override
    public void showMessageInTextView(String message) {
    }

    @Override
    public void setMaxPosition(int count) {
    }



    @Override
    public void visibleButton(boolean isVisible) {
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.buttonSaveBackup:
                presenter.saveBackup();
                break;
            case R.id.buttonRestoreBackup:
                presenter.restoreBackup();
                break;
            case R.id.buttonSaveBackupPhone:
                presenter.saveLocalBackup();
                break;
            case R.id.buttonRestoreBackupPhone:
                presenter.restoreLocalBackup();
                break;
        }
    }
}
