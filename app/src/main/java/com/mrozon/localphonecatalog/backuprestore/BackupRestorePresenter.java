package com.mrozon.localphonecatalog.backuprestore;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mrozon.localphonecatalog.BuildConfig;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.SQLBackupRestore;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.importfile.ImportFileService;
import com.mrozon.localphonecatalog.importfile.ImportFileView;
import com.snatik.storage.Storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.core.content.FileProvider;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

import static com.mrozon.localphonecatalog.backuprestore.BackupRestoreFragment.TAG;
import static com.mrozon.localphonecatalog.db.PhoneCatalogDatabase.DB_NAME;
import static com.mrozon.localphonecatalog.utils.AppConstants.TIMESTAMP_FILE_FORMAT;

@InjectViewState
public class BackupRestorePresenter extends MvpPresenter<BackupRestoreView> {

    private static final String LOCAL_PHONE_CATALOG = "LocalPhoneCatalog";

    private Router router;
    private File file=null;
    private Context context;
    private Storage storage;

    public BackupRestorePresenter() {
        this.router = MyApplication.INSTANCE.getRouter();
//        this.context = MyApplication.INSTANCE;
        if(storage==null)
            storage = new Storage(this.context);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void saveBackup() {
        if(FirebaseAuth.getInstance().getUid()==null)
        {
            getViewState().showMessage(context.getString(R.string.firebase_auth_null));
            return;
        }
        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = storageReference.child(String.format("user/%s/backup/%s.backup", FirebaseAuth.getInstance().getUid(), DB_NAME));

        if(!storage.isDirectoryExists(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath())){
            storage.createDirectory(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath());
        }
        String dirPath = Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath()+ "/Share";
        if(!storage.isDirectoryExists(dirPath)){
            storage.createDirectory(dirPath);
        }
        String backup_file = String.format("%s/%s.backup", dirPath, DB_NAME);
        if(storage.isFileExist(backup_file))
            storage.deleteFile(backup_file);

        Disposable subscribe = Single.fromCallable(() -> {
            PhoneCatalogDatabase.closeDb();
            storage.copy(String.valueOf(context.getDatabasePath(DB_NAME)), backup_file);
            PhoneCatalogDatabase.reopenDb();
            return new Object();
        }).subscribeOn(Schedulers.computation())
                .flatMap((Function<Object, SingleSource<UploadTask.TaskSnapshot>>) o -> {
                    File file = new File(backup_file);
                    Uri uri = Uri.fromFile(file);
                    return RxFirebaseStorage.putFile(riversRef, uri);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(snapshot -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(context.getString(R.string.share_backup));
                    Log.d(TAG, "saveCloudBackup: transferred " + snapshot.getBytesTransferred() + " bytes");
                }, throwable -> {
                    getViewState().hideProgress();
                    Log.d(TAG, "saveCloudBackup: " + throwable.toString());
                    getViewState().showMessage(throwable.toString());
                });

//        if(!storage.isDirectoryExists(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath())){
//            storage.createDirectory(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath());
//        }
//        String dirPath = Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath()+ "/Share";
//        if(!storage.isDirectoryExists(dirPath)){
//            storage.createDirectory(dirPath);
//        }
//        String backup_file = String.format("%s/%s.backup", dirPath, DB_NAME);
//        if(storage.isFileExist(backup_file))
//            storage.deleteFile(backup_file);
//        Completable.fromAction(()-> SQLBackupRestore.backup(context,backup_file))
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe(() -> {
//
//                            Uri uri;
//                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//                                uri = Uri.fromFile(new File(backup_file));
//                            }
//                            else
//                            {
//                                uri = FileProvider.getUriForFile(context,
//                                        BuildConfig.APPLICATION_ID + ".provider",
//                                        new File(backup_file));
//                            }

//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_SEND);
//                            intent.setType("text/plain");
//
//                            intent.putExtra(Intent.EXTRA_SUBJECT, "");
//                            intent.putExtra(Intent.EXTRA_TEXT, "");
//                            intent.putExtra(Intent.EXTRA_STREAM, uri);
//
//                            getViewState().hideProgress();
//                            try {
//                                context.startActivity(Intent.createChooser(intent, context.getString(R.string.share_backup))
//                                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                            } catch (android.content.ActivityNotFoundException ex) {
//                                ex.printStackTrace();
//                            }
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));
    }

    public void restoreBackup() {
        if(FirebaseAuth.getInstance().getUid()==null)
        {
            getViewState().showMessage(context.getString(R.string.firebase_auth_null));
            return;
        }

        StorageReference storageReference = FirebaseStorage.getInstance().getReference();
        String dirPath = Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath()+ "/Share";
        if(!storage.isDirectoryExists(dirPath)){
            storage.createDirectory(dirPath);
        }
        Uri backup_temp_file =  Uri.fromFile(new File(String.format("%s/%s.backup", dirPath, DB_NAME)));
        String backup_temp = String.valueOf(backup_temp_file).replaceFirst("file://","");
        StorageReference riversRef = storageReference.child(String.format("user/%s/backup/%s.backup", FirebaseAuth.getInstance().getUid(), DB_NAME));
        Disposable subscribe = RxFirebaseStorage.getFile(riversRef, backup_temp_file)

                .flatMap(taskSnapshot -> Single.fromCallable(() -> {
                    PhoneCatalogDatabase.closeDb();
                    storage.copy(backup_temp, String.valueOf(context.getDatabasePath(DB_NAME)));
                    PhoneCatalogDatabase.reopenDb();
                    return new Object();
                }).subscribeOn(Schedulers.computation()))
//                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((result) -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(context.getString(R.string.restore_success));
                }, throwable -> {
                    getViewState().hideProgress();
                    Log.d(TAG, "restoreCloudBackup: " + throwable.toString());
                    getViewState().showMessage(throwable.toString());
                });

//        if(!storage.isDirectoryExists(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath())){
//            storage.createDirectory(Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath());
//        }
//        String dirPath = Environment.getExternalStoragePublicDirectory(LOCAL_PHONE_CATALOG).getAbsolutePath()+ "/Share";
//        if(!storage.isDirectoryExists(dirPath)){
//            storage.createDirectory(dirPath);
//        }
//        String backup_file = String.format("%s/%s.backup", dirPath, DB_NAME);
////        if(storage.isFileExist(backup_file))
////            storage.deleteFile(backup_file);
//        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(MyApplication.INSTANCE);
////        db.getOpenHelper().close();
//        PhoneCatalogDatabase.closeDb();
//        ///data/data/com.mrozon.localphonecatalog/databases/phonecatalog.db
//        String db_path = String.format("/data/data/%s/databases/%s", MyApplication.INSTANCE.getPackageName(), DB_NAME);
//
//        Uri backup_temp_file =  Uri.fromFile(new File(String.format("%s/%s.backup", dirPath, DB_NAME)));
//
//
//
//
//        String backup_temp = String.valueOf(backup_temp_file).replaceFirst("file://","");
//
////        File fff = new File(backup_temp);
////        if(fff.isFile())
////            return;
//
////        db.getOpenHelper().getWritableDatabase().query("pragma wal_checkpoint(full)");
//        boolean result = storage.copy(backup_temp,db_path);
////
////        boolean result = storage.copy(db_path,backup_file);
//
//
//        getViewState().showMessage(String.valueOf(result));
//
////        Room.databaseBuilder(MyApplication.INSTANCE.getApplicationContext(), PhoneCatalogDatabase.class, DB_NAME)
////                .setJournalMode(RoomDatabase.JournalMode.TRUNCATE)
////                .build();
//        PhoneCatalogDatabase.reopenDb();
////
////                storage.copy(db_path,backup_file);
//
////        db_path = context.getDatabasePath("phonecatalog.db").getAbsolutePath();
//
//
//
//        ///sdcard/LocalPhoneCatalog/Share/phonecatalog.db.backup
////        Boolean result =storage.deleteFile(db_path);
////        try {
////            String d = Environment.getExternalStorageDirectory().getCanonicalPath();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }
////        boolean result = storage.copy(db_path,db_path+".e");
////
////        int f=1;

    }

    public void saveLocalBackup() {
        DialogProperties properties = new DialogProperties();
        properties.selection_type = DialogConfigs.DIR_SELECT;
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        FilePickerDialog dialog = new FilePickerDialog(context, properties);
        dialog.setTitle(context.getString(R.string.select_dir_backup));
        dialog.setPositiveBtnName(context.getString(R.string.ok));
        dialog.setNegativeBtnName(context.getString(R.string.cancel));
        dialog.setDialogSelectionListener(files -> {
            SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FILE_FORMAT);
            if(files.length<1)
            {
                getViewState().showMessage(context.getString(R.string.file_not_selected));
                return;
            }
            String backup_file = String.format("%s/%s_%s.backup", files[0], DB_NAME, sdf.format(new Date()));
            Completable.fromAction(()->{
                String db_file = String.valueOf(context.getDatabasePath(DB_NAME));
//                db_file = db_file.replace("/user/0/","/data/");
                PhoneCatalogDatabase.closeDb();
                boolean result = storage.copy(db_file,backup_file);
                PhoneCatalogDatabase.reopenDb();
            })
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(__ -> getViewState().showProgress())
                    .subscribe(() -> {
                                getViewState().hideProgress();
                                getViewState().showMessage(context.getString(R.string.share_backup));
                            },
                            (throwable -> {
                                getViewState().hideProgress();
                                getViewState().showMessage(throwable.getMessage());
                            }));
        });
        dialog.show();
    }

    public void restoreLocalBackup() {
        DialogProperties properties = new DialogProperties();
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"backup"};
        FilePickerDialog dialog = new FilePickerDialog(context, properties);
        dialog.setTitle(context.getResources().getString(R.string.select_file_backup));
        dialog.setPositiveBtnName(context.getResources().getString(R.string.ok));
        dialog.setNegativeBtnName(context.getResources().getString(R.string.cancel));
        dialog.setDialogSelectionListener(files -> {
            if(files.length<1){
                getViewState().showMessage(context.getResources().getString(R.string.backup_file_not_select));
            }
            else {
                String backup_file = files[0];
                Completable.fromAction(()-> {
                    String db_file = String.valueOf(context.getDatabasePath(DB_NAME));
                    PhoneCatalogDatabase.closeDb();
                    boolean result = storage.copy(backup_file, db_file);
                    PhoneCatalogDatabase.reopenDb();
                })
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(__ -> getViewState().showProgress())
                        .subscribe(() -> {
                                    getViewState().hideProgress();
                                    getViewState().showMessage(context.getString(R.string.restore_success));
                                },
                                (throwable -> {
                                    getViewState().hideProgress();
                                    getViewState().showMessage(throwable.getMessage());
                                }));
            }
        });
        dialog.show();
    }
}
