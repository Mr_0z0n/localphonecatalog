package com.mrozon.localphonecatalog.backuprestore;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mrozon.localphonecatalog.importfile.ImportFileService;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface BackupRestoreView extends MvpView {
    void showProgress();
    void hideProgress();
    void showMessage(String message);
    void showMessageInTextView(String message);
    void setMaxPosition(int count);
//    void updateProgress(ImportFileService.NumericString numericString);
    void visibleButton(boolean isVisible);
}
