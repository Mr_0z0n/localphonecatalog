package com.mrozon.localphonecatalog.viewphone;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.Screens;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.editphone.EditPhoneView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

import static com.mrozon.localphonecatalog.utils.AppConstants.regexPhone;

@InjectViewState
public class ViewPhonePresenter extends MvpPresenter<ViewPhoneView> {
    private Router router;


    public ViewPhonePresenter() {
        this.router = MyApplication.INSTANCE.getRouter();
    }

    public void toPhoneCatalog() {
        router.navigateTo(new Screens.PhoneCatalog());
    }
}
