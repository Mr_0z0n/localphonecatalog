package com.mrozon.localphonecatalog.viewphone;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.editphone.EditPhonePresenter;
import com.mrozon.localphonecatalog.editphone.EditPhoneView;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ViewPhoneFragment extends MvpAppCompatFragment
        implements ViewPhoneView, View.OnClickListener {

    public static final String TAG = ViewPhoneFragment.class.getSimpleName();

    @BindView(R.id.tvViewPhone)
    TextView tvViewPhone;

    @BindView(R.id.tvViewName)
    TextView tvViewName;

    @BindView(R.id.tvViewAddress)
    TextView tvViewAddress;

    @BindView(R.id.tvViewNote)
    TextView tvViewNote;

    @BindView(R.id.bCloseViewPhone)
    Button bCloseViewPhone;


    @InjectPresenter
    ViewPhonePresenter presenter;

    public static ViewPhoneFragment newInstance(Phone phone) {
        Bundle args = new Bundle();
        args.putSerializable("phone", phone);
        ViewPhoneFragment fragment = new ViewPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void setTitle(int res){
        if(getActivity()!=null)
            getActivity().setTitle(res);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_phone, container, false);
        ButterKnife.bind(this, view);

        setTitle(R.string.view_phone_title);

        Phone phone = null;
        if(getArguments()!=null && !getArguments().isEmpty()) {
            phone = (Phone) getArguments().getSerializable("phone");
        }
        if(phone!=null){
            tvViewAddress.setText(phone.address);
            tvViewName.setText(phone.name);
            tvViewPhone.setText(phone.number);
            tvViewNote.setText(phone.note);

            tvViewAddress.setOnClickListener(this);
            tvViewName.setOnClickListener(this);
            tvViewPhone.setOnClickListener(this);
            tvViewNote.setOnClickListener(this);
        }

        bCloseViewPhone.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.tvViewAddress:
            case R.id.tvViewName:
            case R.id.tvViewPhone:
            case R.id.tvViewNote:
                String mes = ((TextView)v).getText().toString();
                if(mes.isEmpty())
                    break;
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(v.toString(), mes);
                clipboard.setPrimaryClip(clip);
                showMessage(getString(R.string.copy_clipboard));
            break;
            case R.id.bCloseViewPhone:
                presenter.toPhoneCatalog();
                break;
        }
    }



    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }
}
