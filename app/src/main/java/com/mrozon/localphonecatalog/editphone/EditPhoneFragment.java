package com.mrozon.localphonecatalog.editphone;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogPresenter;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogView;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogViewAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EditPhoneFragment extends MvpAppCompatFragment
        implements EditPhoneView, View.OnClickListener {

    public static final String TAG = EditPhoneFragment.class.getSimpleName();

    @BindView(R.id.etEditPhone)
    EditText etEditPhone;

    @BindView(R.id.etEditName)
    EditText etEditName;

    @BindView(R.id.etEditAddress)
    EditText etEditAddress;

    @BindView(R.id.etEditNote)
    EditText etEditNote;

    @BindView(R.id.bCancelPhone)
    Button bCancelPhone;

    @BindView(R.id.bRemovePhone)
    Button bRemovePhone;

    @BindView(R.id.bSavePhone)
    Button bSavePhone;


    @InjectPresenter
    EditPhonePresenter presenter;

    private Disposable phoneDisposable;
    private Disposable nameDisposable;
    private Disposable disposable;


    public static EditPhoneFragment newInstance() {
        Bundle args = new Bundle();
        EditPhoneFragment fragment = new EditPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static EditPhoneFragment newInstance(Phone phone) {
        Bundle args = new Bundle();
        args.putSerializable("phone", phone);
        EditPhoneFragment fragment = new EditPhoneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void setTitle(int res){
        if(getActivity()!=null)
            getActivity().setTitle(res);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_phone, container, false);
        ButterKnife.bind(this, view);

        bSavePhone.setOnClickListener(this);
        bCancelPhone.setOnClickListener(this);
        bRemovePhone.setOnClickListener(this);

        Phone phone = null;
        if(getArguments()==null || getArguments().isEmpty()) {
            //new phone
            bRemovePhone.setVisibility(View.INVISIBLE);
            setTitle(R.string.ic_add_phone);
        }
        else
        {
            //edit existing item
            bRemovePhone.setVisibility(View.VISIBLE);
            setTitle(R.string.ic_edit_phone);
            phone = (Phone) getArguments().getSerializable("phone");
            etEditAddress.setText(phone.address);
            etEditName.setText(phone.name);
            etEditNote.setText(phone.note);
            etEditPhone.setText(phone.number);
            Phone finalPhone = phone;
            bRemovePhone.setOnClickListener(v->{
                presenter.removePhone(finalPhone);
            });
        }

        Observable<String> phoneObservable = RxTextView.textChanges(etEditPhone)
                .skip(phone==null?1:0)
                .observeOn(Schedulers.computation())
                .map(CharSequence::toString)
                .map(s -> presenter.checkInputValue(s));

        phoneDisposable = phoneObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(error -> {
                    if (error.length() > 0)
                        etEditPhone.setError(error);
                });

        Observable<Boolean> nameObservable =RxTextView.textChanges(etEditName)
                .skip(phone==null?1:0)
                .observeOn(Schedulers.computation())
                .map(CharSequence::toString)
                .map(s -> s.trim().isEmpty());

        nameDisposable = nameObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(empty -> {
                    if(empty)
                        etEditName.setError(getString(R.string.empty_field));
                });

        disposable = Observable.combineLatest(phoneObservable, nameObservable,
                (error_phone, empty_name) -> error_phone.isEmpty() && !empty_name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onSaveEnabled -> bSavePhone.setEnabled(onSaveEnabled));

        return view;
    }

    @Override
    public void onDestroyView() {
        if(!phoneDisposable.isDisposed()) phoneDisposable.dispose();
        if(!nameDisposable.isDisposed()) nameDisposable.dispose();
        if(!disposable.isDisposed()) disposable.dispose();
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.bCancelPhone:
                presenter.toPhoneCatalog();
                break;
            case R.id.bSavePhone:
                Phone current_phone = null;
                if(getArguments()!=null && !getArguments().isEmpty()) {
                    current_phone = (Phone) getArguments().getSerializable("phone");
                }
                Phone phone = new Phone(current_phone == null?0:current_phone.id,
                        etEditPhone.getText().toString().toLowerCase(),
                        etEditName.getText().toString().trim().toLowerCase(),
                        etEditAddress.getText().toString().trim().toLowerCase(),
                        etEditNote.getText().toString().trim().toLowerCase());
                presenter.addPhone(phone);
                break;
        }
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }
}
