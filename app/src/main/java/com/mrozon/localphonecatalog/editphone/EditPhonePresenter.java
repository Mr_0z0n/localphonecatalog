package com.mrozon.localphonecatalog.editphone;


import android.app.Person;
import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.Screens;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogView;

import java.security.cert.TrustAnchor;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

import static com.mrozon.localphonecatalog.utils.AppConstants.regexPhone;

@InjectViewState
public class EditPhonePresenter extends MvpPresenter<EditPhoneView> {
    private Router router;

    private Disposable subscribe;

    public EditPhonePresenter() {
        this.router = MyApplication.INSTANCE.getRouter();
    }

    public String checkInputValue(String phone) {
        if(phone.length()<1)
            return MyApplication.INSTANCE.getString(R.string.empty_field);
        //^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$
        Pattern p = Pattern.compile(regexPhone,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(phone);
        if(!m.matches())
            return MyApplication.INSTANCE.getString(R.string.incorrect_value);
        return "";
    }

    public void toPhoneCatalog() {
        router.navigateTo(new Screens.PhoneCatalog());
    }


    private void insertPhone(Phone phone){
        if(!subscribe.isDisposed()) subscribe.dispose();
        Context context = MyApplication.INSTANCE.getApplicationContext();
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);
        Disposable disposable = Completable.fromAction(() ->db.phoneCatalogStore().insertPhone(phone))
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe(__ -> getViewState().showProgress())
            .subscribe(() -> {
                        getViewState().hideProgress();
                        getViewState().showMessage(context.getString(R.string.phone_added));
                        toPhoneCatalog();
                    },
                    (throwable -> {
                        getViewState().hideProgress();
                        getViewState().showMessage(throwable.getMessage());
                    }));
    }

    private void updatePhone(Phone new_phone, Phone current_phone) {
        if(!subscribe.isDisposed()) subscribe.dispose();
        Context context = MyApplication.INSTANCE.getApplicationContext();
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);
        boolean overwrite = false;
        String name = current_phone.name;
        String address = current_phone.address;
        String note = current_phone.note;
        if(name.contains(new_phone.name.toLowerCase()))
        {
            //name already exists - add address
            if(!address.contains(new_phone.address)) {
                address += System.lineSeparator()+new_phone.address;
                overwrite = true;
            }
        }
        else
        {
            //name does not exists - add name and address
            overwrite = true;
            name += System.lineSeparator()+new_phone.name;
            if(!address.contains(new_phone.address))
                address += System.lineSeparator()+new_phone.address;
        }
        if(!note.equalsIgnoreCase(new_phone.note)) {
            overwrite = true;
            note = new_phone.note;
        }
        if(overwrite)
        {
            Phone updated = new Phone(current_phone.id,current_phone.number,name,address, note);
            Disposable disposable = Completable.fromAction(() -> db.phoneCatalogStore().updatePhone(updated))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(context.getString(R.string.phone_updated));
                            toPhoneCatalog();
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
        }

    }

    public void addPhone(Phone phone) {
        Context context = MyApplication.INSTANCE.getApplicationContext();
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);

        subscribe = db.phoneCatalogStore().selectPhone(phone.number)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((phones) -> {
                            getViewState().hideProgress();
                            if(phones.size()>0) {
                                updatePhone(phone, phones.get(0));
//                                getViewState().showMessage("update");
                            }
                            else{
                                insertPhone(phone);
//                                getViewState().showMessage("insert");
                            }
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));


//        Disposable subscribe = db.phoneCatalogStore().selectPhone(phone.number)
//                .subscribeOn(Schedulers.computation())
//                .toObservable()
//                .flatMap((Function<List<Phone>, Observable<Integer>>)  phones->{
//                    if(phones.size()>0) {
//                        Phone current = phones.get(0);
//                        db.phoneCatalogStore().updatePhone(phone);
//                        return Observable.just(phones.size());
//                    }
//                    else
//                    {
//                        db.phoneCatalogStore().insertPhone(phone);
//                        return Observable.just(phones.size());
//                    }
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe((count) -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(String.valueOf(count));
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));

//                .map(phones -> )
//                .flatMap((Function<List<Phone>, Single<Integer>>)
//                        phones-> Single.just(phones.size()))

//        Disposable disposable = db.phoneCatalogStore().selectPhone(phone.number)
//                .subscribeOn(Schedulers.computation())
//                .toObservable()
//                .singleElement()
//                .flatMap((Function<List<Phone>, Single<Object>>)
//                        phones->{
//                            if(phones.size()>0){
//                                //edit
//                                Boolean overwrite = false;
//                                Phone current = phones.get(0);
//                                if(current.name.contains(phone.name.toLowerCase()))
//                                {
//                                    //name already exists - add address
//                                    if(!current.address.contains(phone.address)) {
//                                        current.address.concat(System.lineSeparator()).concat(phone.address);
//                                        overwrite = true;
//                                    }
//                                }
//                                else
//                                {
//                                    //name does not exists - add name and address
//                                    overwrite = true;
//                                    current.name.concat(System.lineSeparator()).concat(phone.name);
//                                    if(!current.address.contains(phone.address))
//                                        current.address.concat(System.lineSeparator()).concat(phone.address);
//                                }
////                                Completable.fromAction(() -> db.phoneCatalogStore().updatePhone(current));
//                                if(current.note.equalsIgnoreCase(phone.note)) {
//                                    overwrite = true;
////                                    current.note. = phone.note;
//                                }
//                                if(overwrite)
////                                    db.phoneCatalogStore().updatePhone(current);
//                                {
//                                    Single<Object> updateObservable = Single.create(emitter -> {
//                                        try {
//                                            db.phoneCatalogStore().updatePhone(current);
//                                            emitter.onSuccess(new Object());
//                                        } catch (Exception e) {
//                                            emitter.onError(e);
//                                        }
//                                    });
//                                    return updateObservable;
//                                }
////                                    return Completable.fromAction(() -> db.phoneCatalogStore().updatePhone(current)).toObservable();
//                            }
//                            else
//                            {
//                                //add
////                                return Completable.fromAction(() -> db.phoneCatalogStore().insertPhone(phone)).toObservable();
////                                db.phoneCatalogStore().insertPhone(phone);
//                                Single<Object> insertObservable = Single.create(emitter -> {
//                                    try {
//                                        db.phoneCatalogStore().insertPhone(phone);
//                                        emitter.onSuccess(new Object());
//                                    } catch (Exception e) {
//                                        emitter.onError(e);
//                                    }
//                                });
//                                return insertObservable;
//                            }
//                            return Single.;
//                        })
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe((object) -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(context.getString(R.string.phone_added));
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));

    }


    public void removePhone(Phone phone) {
        Context context = MyApplication.INSTANCE.getApplicationContext();
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);
        Disposable disposable = Completable.fromAction(() ->db.phoneCatalogStore().deletePhone(phone))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe(() -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(context.getString(R.string.phone_deleted));
                            toPhoneCatalog();
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }
}
