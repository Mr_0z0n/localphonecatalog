package com.mrozon.localphonecatalog.editphone;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface EditPhoneView extends MvpView {
    void showProgress();
    void hideProgress();
    void showMessage(String message);
}
