package com.mrozon.localphonecatalog.home;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpPresenter;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.Screens;

import java.io.File;

import androidx.annotation.NonNull;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

import static com.mrozon.localphonecatalog.home.HomeActivity.RC_SIGN_IN;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    private Router router;
    private Activity activity;

    HomePresenter() {
        router = MyApplication.INSTANCE.getRouter();
    }

    void onShowPhoneCatalog() {
        router.replaceScreen(new Screens.PhoneCatalog());
    }

    void onFirstShowPhoneCatalog() {
        router.newRootScreen(new Screens.PhoneCatalog());
    }

    public void onShowCallList() {
        router.replaceScreen(new Screens.CallList());
    }

    void selectFileDialog(Activity activity) {
        Context context =  MyApplication.INSTANCE.getBaseContext();
        DialogProperties properties = new DialogProperties();
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"csv"};
        FilePickerDialog dialog = new FilePickerDialog(activity, properties);
        dialog.setTitle(context.getResources().getString(R.string.select_import_file));
        dialog.setPositiveBtnName(context.getResources().getString(R.string.ok));
        dialog.setNegativeBtnName(context.getResources().getString(R.string.cancel));
        dialog.setDialogSelectionListener(files -> {
            if(files.length<1){
                getViewState().showMessage(context.getResources().getString(R.string.file_not_select));
            }
            else {
                router.navigateTo(new Screens.ImportFile(files[0]));
            }
        });
        dialog.show();

    }

    public void onBackupRestoreShow() {
        router.navigateTo(new Screens.BackupRestore());
    }

    public void sign(MvpAppCompatActivity activity, GoogleSignInClient mGoogleSignInClient, FirebaseAuth mAuth) {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser==null){
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            this.activity = activity;
            activity.startActivityForResult(signInIntent, RC_SIGN_IN);
            getViewState().closeDrawer();
        }
        else
        {
            mGoogleSignInClient.signOut();
            mAuth.signOut();
            getViewState().updateUserInfo(null);
            getViewState().closeDrawer();
            getViewState().showMessage(activity.getString(R.string.log_out));
        }
    }

    public void getResultGoogleSignIn(Task<GoogleSignInAccount> task, FirebaseAuth mAuth) {
        try {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = task.getResult(ApiException.class);
            firebaseAuthWithGoogle(account, mAuth);
        } catch (ApiException e) {
            getViewState().showMessage(e.getMessage());
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct, final FirebaseAuth mAuth) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, updateIndicatorType UI with the signed-in user's information
                    Log.d(HomeActivity.class.getSimpleName(), "signInWithCredential:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user != null) {
                        //getViewState().showError(user.getDisplayName());
                        getViewState().showGreating(activity.getString(R.string.greating,user.getDisplayName()));
                    }
                    getViewState().updateUserInfo(user);
                } else {
                    // If sign in fails, display a message to the user.
                    getViewState().showMessage("Authentication Failed: "+task.getException().getMessage());
                }
            }
        });

    }

    public void updateUserInfo(FirebaseUser currentUser) {
        getViewState().updateUserInfo(currentUser);
    }


}
