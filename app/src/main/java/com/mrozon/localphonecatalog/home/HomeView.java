package com.mrozon.localphonecatalog.home;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.google.firebase.auth.FirebaseUser;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface HomeView extends MvpView {
    void showMessage(String message);
    void updateUserInfo(FirebaseUser user);
    void closeDrawer();
    @StateStrategyType(SkipStrategy.class)
    void showGreating(String message);
}
