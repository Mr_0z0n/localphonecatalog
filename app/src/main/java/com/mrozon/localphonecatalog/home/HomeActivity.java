package com.mrozon.localphonecatalog.home;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.rx.RxPermissions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class HomeActivity extends MvpAppCompatActivity
        implements HomeView, NavigationView.OnNavigationItemSelectedListener {

    public static final int RC_SIGN_IN = 9001;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;


    @InjectPresenter
    HomePresenter mHomePresenter;

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private Navigator navigator;
    private NavigatorHolder navigatorHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        navigator = new SupportAppNavigator(this, getSupportFragmentManager(), R.id.contentFrame);
        navigatorHolder = MyApplication.INSTANCE.getNavigatorHolder();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
        //get version
        View headerLayout = navigationView.getHeaderView(0);
        TextView textVersion = headerLayout.findViewById(R.id.textVersion);
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            textVersion.setText(getString(R.string.nav_header_version,version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        ImageView imageSign = headerLayout.findViewById(R.id.buttonSign);
        imageSign.setOnClickListener(v -> mHomePresenter.sign(HomeActivity.this, mGoogleSignInClient,mAuth));

        if(getSupportFragmentManager().getFragments().isEmpty())
            mHomePresenter.onFirstShowPhoneCatalog();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            mHomePresenter.getResultGoogleSignIn(GoogleSignIn.getSignedInAccountFromIntent(data), mAuth);
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mHomePresenter.updateUserInfo(mAuth.getCurrentUser());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_phone_catalog:
                mHomePresenter.onShowPhoneCatalog();
                break;
            case R.id.nav_load_from_file:
                checkPermissions();
                break;
            case R.id.nav_backup_restore:
                mHomePresenter.onBackupRestoreShow();
                break;
            case R.id.nav_call_list:
                checkPermissionsCallList();
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkPermissionsCallList() {
        Disposable subscribe = new RxPermissions(this).request(Manifest.permission.READ_CALL_LOG)
                .subscribe(result -> {
                    mHomePresenter.onShowCallList();
                }, throwable -> {
                    final PermissionResult result = ((RxPermissions.Error) throwable).getResult();
                    if(result.hasDenied()) {
                        //permission denied, but you can ask again, eg:
                        new AlertDialog.Builder(this)
                                .setMessage(R.string.accept_permissions)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    result.askAgain();
                                }) // ask again
                                .setNegativeButton(R.string.no, (dialog, which) -> {
                                    dialog.dismiss();
                                })
                                .show();
                    }
//                    if(result.hasForeverDenied()) {
//                        // you need to open setting manually if you really need it
////                        result.goToSettings();
//                    }
                });
    }

    private void checkPermissions(){
        Disposable subscribe = new RxPermissions(this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(result -> {
                    mHomePresenter.selectFileDialog(this);
                }, throwable -> {
                    final PermissionResult result = ((RxPermissions.Error) throwable).getResult();
                    if(result.hasDenied()) {
                        //permission denied, but you can ask again, eg:
                        new AlertDialog.Builder(this)
                                .setMessage(R.string.accept_permissions)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    result.askAgain();
                                }) // ask again
                                .setNegativeButton(R.string.no, (dialog, which) -> {
                                    dialog.dismiss();
                                })
                                .show();
                    }
//                    if(result.hasForeverDenied()) {
//                        // you need to open setting manually if you really need it
////                        result.goToSettings();
//                    }
                });
    }

    @Override
    public void showMessage(String message) {
            Snackbar.make(navigationView, message, Snackbar.LENGTH_LONG)
                    .show();
    }

    @Override
    public void updateUserInfo(FirebaseUser user) {
        View headerLayout = navigationView.getHeaderView(0);
        ImageView imageSign = headerLayout.findViewById(R.id.buttonSign);
        ImageView imagePhoto = headerLayout.findViewById(R.id.imageView);
        TextView tvUserName = headerLayout.findViewById(R.id.textUserName);
        TextView tvEmail = headerLayout.findViewById(R.id.textEmail);
        if(user==null){
            tvUserName.setText(R.string.nav_header_title);
            tvEmail.setText(R.string.nav_header_subtitle);
            imageSign.setImageResource(R.drawable.ic_login);
            imagePhoto.setImageResource(R.drawable.ic_add_user);
        }
        else
        {
            tvUserName.setText(user.getDisplayName());
            tvEmail.setText(user.getEmail());
            imageSign.setImageResource(R.drawable.ic_logout);
            Uri photoUrl = user.getPhotoUrl();
            if (photoUrl == null) {
                imagePhoto.setImageResource(R.drawable.ic_no_photo);
            }
            else
            {
                showPhoto(imagePhoto,0,photoUrl);
            }
        }
    }

    private void showPhoto(ImageView imagePhoto, int res, Uri uri){
        if(res>0){
            Picasso.with(HomeActivity.this)
                    .load(res)
                    .resize(100, 100)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(imagePhoto);
        }
        else
        {
            Picasso.with(HomeActivity.this)
                    .load(uri)
                    .resize(100, 100)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .into(imagePhoto);
        }
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void showGreating(String message) {
        Snackbar.make(navigationView, message, Snackbar.LENGTH_LONG)
                .show();
    }

    public static class CircleTransform implements Transformation {

        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }
}
