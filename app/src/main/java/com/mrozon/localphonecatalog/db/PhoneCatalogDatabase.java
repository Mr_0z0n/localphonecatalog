package com.mrozon.localphonecatalog.db;

import android.content.Context;

import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.db.model.Phone;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities={Phone.class}, version=1)
public abstract class PhoneCatalogDatabase extends RoomDatabase {

    public abstract PhoneCatalogStore phoneCatalogStore();

    public static final String DB_NAME="phonecatalog.db";
    private static volatile PhoneCatalogDatabase INSTANCE=null;

    public synchronized static PhoneCatalogDatabase get(Context ctxt) {
        if (INSTANCE == null) {
            INSTANCE = create(ctxt);
        }
        return (INSTANCE);
    }

    private static PhoneCatalogDatabase create(Context ctxt) {
        Builder<PhoneCatalogDatabase> b;

        b = Room.databaseBuilder(ctxt.getApplicationContext(), PhoneCatalogDatabase.class, DB_NAME);
//    b.addMigrations(Migrations.FROM_1_TO_2);
        return (b.build());
    }

    public static void reopenDb(){
        Context ctxt = MyApplication.INSTANCE;
        Builder<PhoneCatalogDatabase> b;
        b = Room.databaseBuilder(ctxt.getApplicationContext(), PhoneCatalogDatabase.class, DB_NAME);
//        .setJournalMode(RoomDatabase.JournalMode.TRUNCATE);
        INSTANCE = b.build();
    }

    public static  void closeDb(){
        if (INSTANCE != null)
            INSTANCE.getOpenHelper().close();
    }

}
