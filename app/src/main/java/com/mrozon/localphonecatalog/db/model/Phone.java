package com.mrozon.localphonecatalog.db.model;

import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.utils.rvpattern.IBaseListItem;

import java.io.Serializable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "phones")
public class Phone implements Serializable, IBaseListItem {
    @PrimaryKey(autoGenerate=true)
    @ColumnInfo(name="phone_id")
    public final int id;
    @ColumnInfo(name="phone_number")
    public final String number;
    @ColumnInfo(name="phone_name")
    public final String name;
    @ColumnInfo(name="phone_address")
    public final String address;
    @ColumnInfo(name="phone_note")
    public final String note;


    public Phone(int id, String number, String name, String address, String note) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.address = address;
        this.note = note;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_phone;
    }
}
