package com.mrozon.localphonecatalog.db.model;

import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.utils.rvpattern.IBaseListItem;

import java.util.Date;

public class Call implements IBaseListItem {

    private String callNumber;
    private int callType;
    private Date callDate;
    private String callDuration;
    private Phone phone;

    public Call(String callNumber, int callType, Date callDate, String callDuration, Phone phone) {
        this.callNumber = callNumber;
        this.callType = callType;
        this.callDate = callDate;
        this.callDuration = callDuration;
        this.phone = phone;
    }

    public String getCallNumber() {
        return callNumber;
    }

    public int getCallType() {
        return callType;
    }

    public Date getCallDate() {
        return callDate;
    }

    public String getCallDuration() {
        return callDuration;
    }

    public Phone getPhone() {
        return phone;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_call;
    }
}
