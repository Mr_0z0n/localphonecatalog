package com.mrozon.localphonecatalog.db;

import com.mrozon.localphonecatalog.db.model.Phone;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Completable;
import io.reactivex.Flowable;

@Dao
public interface PhoneCatalogStore {

//    @Query("pragma wal_checkpoint(full)")
//    void checkpoint();

    //Phone
    @Query("SELECT * FROM phones limit :limit")
    Flowable<List<Phone>> selectPhones(int limit);

    @Query("SELECT * from phones where phone_number = :phone LIMIT 1")
    Phone selectPhoneByNumber(String phone);

    @Query("SELECT * FROM phones where phone_number like :filter OR phone_name like :filter limit :limit")
    Flowable<List<Phone>> selectPhonesByFilter(String filter, int limit);

    @Query("SELECT * FROM phones where phone_number=:phone")
    Flowable<List<Phone>> selectPhone(String phone);

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertPhone(Phone... phones);

    @Update
    void updatePhone(Phone... phones);

    @Delete
    void deletePhone(Phone... phones);

}
