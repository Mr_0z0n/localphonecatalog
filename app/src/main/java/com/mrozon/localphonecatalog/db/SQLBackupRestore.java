package com.mrozon.localphonecatalog.db;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import com.snatik.storage.Storage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.sqlite.db.SupportSQLiteDatabase;


import static com.mrozon.localphonecatalog.utils.AppConstants.TIMESTAMP_FILE_FORMAT;

public class SQLBackupRestore {

    public static void backup(Context context, String fname){
        PhoneCatalogDatabase phoneCatalogDatabase = PhoneCatalogDatabase.get(context);
        Storage storage = new Storage(context);
        SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FILE_FORMAT, Locale.getDefault());

        storage.createFile(fname,"-- Backup Script started at "+sdf.format(new Date())+"\n");

        SupportSQLiteDatabase db = phoneCatalogDatabase.getOpenHelper().getReadableDatabase();

        storage.appendFile(fname,"\n-- TABLE INDICATORS");
        Cursor cursor = db.query("SELECT * FROM phones");
//        CREATE TABLE `phones` (`phone_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
//          `phone_number` TEXT, `phone_name` TEXT, `phone_address` TEXT, `phone_note` TEXT)
        while (cursor.moveToNext()){
            long phone_id = cursor.getLong(cursor.getColumnIndex("phone_id"));
            String phone_number = cursor.getString(cursor.getColumnIndex("phone_number"));
            phone_number = phone_number.trim().replace("\n","'||char(10)||'");
            String phone_name = cursor.getString(cursor.getColumnIndex("phone_name"));
            phone_name = phone_name.trim().replace("\n","'||char(10)||'");
            String phone_address = cursor.getString(cursor.getColumnIndex("phone_address"));
            phone_address = phone_address.trim().replace("\n","'||char(10)||'");
            String phone_note = cursor.getString(cursor.getColumnIndex("phone_note"));
            phone_note = phone_note.trim().replace("\n","'||char(10)||'");
            String row = String.format("INSERT INTO indicators VALUES(%d, '%s','%s', '%s','%s');",
                    phone_id, phone_number, phone_name,phone_address,phone_note);
//            Log.d("SQLBackupRestore", "backup: "+ row);
            storage.appendFile(fname,row);
        }
        cursor.close();

        storage.appendFile(fname,"\n-- Backup Script completed at "+sdf.format(new Date())+"\n");


    }

    public static void restore(Context context, String fname) {
        PhoneCatalogDatabase phoneCatalogDatabase = PhoneCatalogDatabase.get(context);
        phoneCatalogDatabase.clearAllTables();

        SupportSQLiteDatabase db = phoneCatalogDatabase.getOpenHelper().getWritableDatabase();
//        int res = db.delete("sqlite_sequence",null,null);
//        Log.d("RESTORE", "restore: "+res);

        Storage storage = new Storage(context);

        String[] commands = storage.readTextFile(fname).split("\n");
        for (String command: commands) {
            if(command.length()==0)
                continue;
            if(command.startsWith("--"))
                continue;
            try {
                db.execSQL(command);
//                Log.d("SQLBackupRestore", "SQLiteConstraintException: "+command);
            }
            catch (SQLiteConstraintException e){
                Log.e("SQLBackupRestore", "SQLiteConstraintException: "+command);
            }
        }

        Log.d("RESTORE", "restore: "+commands.length);

    }
}
