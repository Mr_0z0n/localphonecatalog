package com.mrozon.localphonecatalog.phonecatalog;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mrozon.localphonecatalog.db.model.Phone;

import java.util.List;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface PhoneCatalogView extends MvpView {
    void updateList(List<Phone> list);
    void showMessage(String message);
    void showProgress();
    void hideProgress();
}
