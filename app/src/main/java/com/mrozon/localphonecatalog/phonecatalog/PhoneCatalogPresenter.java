package com.mrozon.localphonecatalog.phonecatalog;


import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.Screens;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Phone;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class PhoneCatalogPresenter extends MvpPresenter<PhoneCatalogView> {
    private Router router;

    public PhoneCatalogPresenter() {
        router = MyApplication.INSTANCE.getRouter();
    }

    public void addNewPhone() {
        router.navigateTo(new Screens.AddPhone());
    }

    void getItems() {
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(MyApplication.INSTANCE.getApplicationContext());
        final Disposable items = db.phoneCatalogStore().selectPhones(100)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__-> getViewState().showProgress())
                .subscribe((List<Phone> phones) -> {
                    getViewState().updateList(phones);
                    getViewState().hideProgress();
                    getViewState().showMessage("Ok");
                }, throwable -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(throwable.getMessage());
                });
    }

    void getPhoneByFilter(String s) {
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(MyApplication.INSTANCE.getApplicationContext());
        final Disposable items = db.phoneCatalogStore().selectPhonesByFilter('%'+s.toLowerCase()+'%',100)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__-> getViewState().showProgress())
                .subscribe((List<Phone> phones) -> {
                    getViewState().updateList(phones);
                    getViewState().hideProgress();
//                    getViewState().showMessage("Ok");
                }, throwable -> {
                    getViewState().hideProgress();
                    getViewState().showMessage(throwable.getMessage());
                });
    }

    public void editPhone(Phone phone) {
        router.navigateTo(new Screens.EditPhone(phone));
    }

    public void showPhone(Phone phone) {
        router.navigateTo(new Screens.ViewPhone(phone));
    }

//    void deleteItem(YTVideo video) {
//        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(this.context);
//        Disposable disposable = Completable.fromAction(() -> db.mLearningStore().deleteYTVideo(video))
//                .subscribeOn(Schedulers.computation())
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnSubscribe(__ -> getViewState().showProgress())
//                .subscribe(() -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(context.getString(R.string.video_deleted));
//                        },
//                        (throwable -> {
//                            getViewState().hideProgress();
//                            getViewState().showMessage(throwable.getMessage());
//                        }));
//    }

}
