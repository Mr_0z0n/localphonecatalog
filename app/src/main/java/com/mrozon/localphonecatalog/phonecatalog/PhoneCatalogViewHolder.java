package com.mrozon.localphonecatalog.phonecatalog;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrozon.localphonecatalog.R;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

class PhoneCatalogViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvPhone)
    TextView  tvPhone;

    @BindView(R.id.ivAddres)
    ImageView ivAddres;

    @BindView(R.id.ivNote)
    ImageView ivNote;

    @BindView(R.id.cvPhone)
    CardView cvPhone;

    PhoneCatalogViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
