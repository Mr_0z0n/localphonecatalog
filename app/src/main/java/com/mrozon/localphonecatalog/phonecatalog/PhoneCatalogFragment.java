package com.mrozon.localphonecatalog.phonecatalog;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.utils.rvpattern.IBaseListItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PhoneCatalogFragment extends MvpAppCompatFragment
        implements PhoneCatalogView {

    public static final String TAG = PhoneCatalogFragment.class.getSimpleName();

    @BindView(R.id.rvItems)
    RecyclerView rvItems;

    @BindView(R.id.etSearchPattern)
    EditText etSearchPattern;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectPresenter
    public PhoneCatalogPresenter presenter;

    private PhoneCatalogViewAdapter adapter;

    public static PhoneCatalogFragment newInstance() {
        Bundle args = new Bundle();
        PhoneCatalogFragment fragment = new PhoneCatalogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone_catalog, container, false);
        ButterKnife.bind(this, view);

        setHasOptionsMenu(true);



        adapter = new PhoneCatalogViewAdapter(getContext(), new PhoneCatalogViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(Phone phone) {
                presenter.showPhone(phone);
            }

            @Override
            public void onItemLongClick(Phone phone) {
                presenter.editPhone(phone);
            }
        });

        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        rvItems.setAdapter(adapter);

//        presenter.setContext(getContext());
        presenter.getItems();

        /*Observable<String> phoneObservable =*/
        Disposable subscribe = RxTextView.textChanges(etSearchPattern)
                .skip(1)
                .observeOn(Schedulers.computation())
                .map(CharSequence::toString)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> showProgress())
                .subscribe((String filter) -> presenter.getPhoneByFilter(filter),
                        throwable -> showMessage(throwable.getMessage()));


//        etSearchPattern.clearFocus();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            etSearchPattern.setShowSoftInputOnFocus(false);
//        }
//        InputMethodManager imm = (InputMethodManager) MyApplication.INSTANCE.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_phone_catalog, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.ic_add_phone) {
            presenter.addNewPhone();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateList(List<Phone> list) {
        adapter.clearAll();
        for (Phone item: list) {
            adapter.add(item);
        }
    }

    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
}
