package com.mrozon.localphonecatalog.phonecatalog;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.utils.rvpattern.RVAdapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;

public class PhoneCatalogViewAdapter extends RVAdapter {

    private  ItemClickListener itemClickListener = null;
    private Context context;

    public interface ItemClickListener {
        void onItemClick(Phone phone);
        void onItemLongClick(Phone phone);
    }

    public PhoneCatalogViewAdapter(Context context, ItemClickListener itemClickListener) {
        super();
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PhoneCatalogViewHolder(inflateByViewType(context,viewType,parent));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof PhoneCatalogViewHolder){
            Phone phone = (Phone) items.get(position);
            PhoneCatalogViewHolder catalogViewHolder = (PhoneCatalogViewHolder) holder;
            catalogViewHolder.tvName.setText(phone.name);
            catalogViewHolder.tvPhone.setText(phone.number);
            catalogViewHolder.ivAddres.setVisibility(phone.address.isEmpty()? View.INVISIBLE:View.VISIBLE);
            catalogViewHolder.ivNote.setVisibility(phone.note.isEmpty()? View.INVISIBLE:View.VISIBLE);
            catalogViewHolder.cvPhone.setOnClickListener(v->{
                if(itemClickListener!=null)
                    itemClickListener.onItemClick(phone);
            });
            catalogViewHolder.cvPhone.setOnLongClickListener(v -> {
                if(itemClickListener!=null)
                    itemClickListener.onItemLongClick(phone);
                return false;
            });
        }
    }
}
