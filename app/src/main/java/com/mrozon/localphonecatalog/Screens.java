package com.mrozon.localphonecatalog;

import android.print.PrintJob;

import com.mrozon.localphonecatalog.backuprestore.BackupRestoreFragment;
import com.mrozon.localphonecatalog.calllist.CallListFragment;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.editphone.EditPhoneFragment;
import com.mrozon.localphonecatalog.importfile.ImportFileFragment;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogFragment;
import com.mrozon.localphonecatalog.viewphone.ViewPhoneFragment;

import androidx.fragment.app.Fragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class PhoneCatalog extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return PhoneCatalogFragment.newInstance();
        }
    }

    public static final class AddPhone extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return EditPhoneFragment.newInstance();
        }
    }

    public static final class EditPhone extends SupportAppScreen {

        private Phone phone;

        public EditPhone(Phone phone) {
            this.phone = phone;
        }

        @Override
        public Fragment getFragment() {
            return EditPhoneFragment.newInstance(phone);
        }
    }

    public static final class ImportFile extends SupportAppScreen {

        private String file;

        public ImportFile(String file) {
            this.file = file;
        }

        @Override
        public Fragment getFragment() {
            return ImportFileFragment.newInstance(file);
        }
    }

    public static final class ViewPhone extends SupportAppScreen {

        private Phone phone;

        public ViewPhone(Phone phone) {
            this.phone = phone;
        }

        @Override
        public Fragment getFragment() {
            return ViewPhoneFragment.newInstance(phone);
        }
    }

    public static final class BackupRestore extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return BackupRestoreFragment.newInstance();
        }
    }

    public static final class CallList extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return CallListFragment.newInstance();
        }
    }

}
