package com.mrozon.localphonecatalog.utils.rvpattern;

public interface IBaseListItem {
    int getLayoutId();
}
