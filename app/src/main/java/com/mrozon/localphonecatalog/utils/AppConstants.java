package com.mrozon.localphonecatalog.utils;

public final class AppConstants {

    public static final String TIMESTAMP_FORMAT = "dd MMM HH:mm:ss";//"dd-MM-yyyy HH:mm:ss";
    public static final String TIMESTAMP_FILE_FORMAT = "dd_MM_yyyy_HH_mm";//"yyyyMMdd_HHmmss";
    public static final String TIMESTAMP_LABEL_FORMAT = "HH:mm dd-MM-yyyy";
    public static final String regexPhone="^(8|\\+7)\\d{10}$";

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}