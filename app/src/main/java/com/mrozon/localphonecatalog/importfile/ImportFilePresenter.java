package com.mrozon.localphonecatalog.importfile;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Phone;

import org.reactivestreams.Publisher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import androidx.fragment.app.FragmentActivity;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class ImportFilePresenter extends MvpPresenter<ImportFileView> {

    private Router router;
    private File file=null;
    private Context context;

    private static Thread thread;
    private Boolean interruptProccess = false;

    public ImportFilePresenter() {
        this.router = MyApplication.INSTANCE.getRouter();
        this.context = MyApplication.INSTANCE.getApplicationContext();
    }


    private boolean bound = false;
    private ImportFileService importFileService=null;
    private ServiceConnection sc=null;

    public void importCSV(File file) {
//        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
//            String line;
//            while ((line = br.readLine()) != null) {
//                // process the line.
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        this.file = file;

        Single<Integer> countStringInFile = Single.create(emitter -> {
            BufferedReader br = null;
            try {
                int count=0;
                br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    count++;
                }
                emitter.onSuccess(count);
            } catch (Exception e) {
                emitter.onError(e);
            } finally {
                if (br != null) {
                    br.close();
                }
            }
        });

        Disposable subscribe = countStringInFile.subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((count) -> {
                            getViewState().hideProgress();
                            getViewState().setMaxPosition(count);
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }



//    private Flowable<String> selectPhone2(PhoneCatalogDatabase db, Phone phone){
//        return db.phoneCatalogStore().selectPhone(phone.number)
//                .flatMap((Function<List<Phone>,Single<String>>)phones -> {
//                    if(phones.size()>0) {
//                    //exists
//                    return Single.create(emmiter -> {
//                        Phone updated = needUpdate(phone, phones.get(0));
//                        if(updated!=null)
//                            db.phoneCatalogStore().updatePhone(updated);
//                        emmiter.onSuccess(phone.number+" updated");
//                    });
//                }
//                else{
//                    //new
//                    return Single.create(emmiter -> {
//                        try {
//                            db.phoneCatalogStore().insertPhone(phone);
//                            emmiter.onSuccess(phone.number+" inserted");
//                        }
//                        catch (Exception e){
//                            emmiter.onError(e);
//                        }
//                    });
//                }});
//    }

    private Flowable<String> selectPhone5(PhoneCatalogDatabase db, Phone phone){
        return db.phoneCatalogStore().selectPhone(phone.number)
                .concatMap(phones -> {if(phones.size()>0) {
                    //exists
                    Phone updated = needUpdate(phone, phones.get(0));
                    if(updated!=null){
                        db.phoneCatalogStore().updatePhone(updated);
                        return Flowable.just(phone.number + " updated");
                    }
                    else
                    {
                        return Flowable.just(phone.number + " missed");
                    }
                }
                else{
                    //new
                    db.phoneCatalogStore().insertPhone(phone);
                    return Flowable.just(phone.number+" inserted");
                }});
    }

    private Flowable<String> selectPhone4(PhoneCatalogDatabase db, Phone phone){
        return Flowable.just(db.phoneCatalogStore().selectPhoneByNumber(phone.number))
                .concatMap(p -> {
                    if(p==null) {
                        db.phoneCatalogStore().insertPhone(phone);
                        return Flowable.just(phone.number+" inserted");
                    }
                    else
                    {
                        Phone updated = needUpdate(phone, p);
                        if(updated!=null) {
                            db.phoneCatalogStore().updatePhone(updated);
                            return Flowable.just(phone.number + " updated");
                        }
                        return Flowable.just(phone.number + " missed");
                    }
                });
    }

    private Flowable<String> selectPhone3(PhoneCatalogDatabase db, Phone phone){
        return db.phoneCatalogStore().selectPhone(phone.number)
                .flatMap(phones -> Flowable.create(emmiter -> {
                    if(phones.size()>0){
                        Phone updated = needUpdate(phone, phones.get(0));
                        if(updated!=null){
                            db.phoneCatalogStore().updatePhone(updated);
                            emmiter.onNext(phone.number+" updated");
                        }
                    }
                    else
                    {
                        db.phoneCatalogStore().insertPhone(phone);
                        emmiter.onNext(phone.number+" inserted");
                    }

                    emmiter.onComplete();
                }, BackpressureStrategy.DROP));
    }

    private Flowable<String> selectPhone(PhoneCatalogDatabase db, Phone phone){
        return db.phoneCatalogStore().selectPhone(phone.number)
            .concatMap(phones -> {if(phones.size()>0) {
                //exists
                return Flowable.create(emmiter -> {
                    Phone updated = needUpdate(phone, phones.get(0));
                    if(updated!=null) {
                        db.phoneCatalogStore().updatePhone(updated);
                        emmiter.onNext(phone.number+" updated");
                    }
                    else {
                        emmiter.onNext(phone.number + " missed");
                    }
                    emmiter.onComplete();
                }, BackpressureStrategy.DROP);
            }
            else{
                //new
                return Flowable.create(emmiter -> {
                    try {
                        db.phoneCatalogStore().insertPhone(phone);
                        emmiter.onNext(phone.number+" inserted");
                        emmiter.onComplete();
                    }
                    catch (Exception e){
                        emmiter.onError(e);
                    }
                }, BackpressureStrategy.DROP);
            }});
    }

    private Phone needUpdate(Phone new_phone, Phone current_phone){
        boolean overwrite = false;

        String name = current_phone.name;
        String address = current_phone.address;
        String note = current_phone.note;
        if(name.contains(new_phone.name.toLowerCase()))
        {
            //name already exists - add address
            if(!address.contains(new_phone.address)) {
                address += System.lineSeparator()+new_phone.address;
                overwrite = true;
            }
        }
        else
        {
            //name does not exists - add name and address
            overwrite = true;
            name += System.lineSeparator()+new_phone.name;
            if(!address.contains(new_phone.address))
                address += System.lineSeparator()+new_phone.address;
        }
        if(!note.equalsIgnoreCase(new_phone.note)) {
            overwrite = true;
            note = new_phone.note;
        }
        if(overwrite) {
            return new Phone(current_phone.id, current_phone.number, name, address, note);
        }
        return null;
        }


    public void stopImport() {
        if(importFileService!=null)
            importFileService.stopImport();
    }

    public void importCSVAsService(Activity activity, File file) {
        Intent intent = new Intent(MyApplication.INSTANCE, ImportFileService.class);
        sc = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                importFileService = ((ImportFileService.MyBinder)binder).getService();
                importFileService.setListener(new ImportFileService.ImportFileServiceListener() {
                    @Override
                    public void onChangeVisibleButton(boolean isVisible) {
                        activity.runOnUiThread(() -> {
                            getViewState().visibleButton(isVisible);
                        });
                    }

                    @Override
                    public void onError(String message) {
                        activity.runOnUiThread(() -> {
                            getViewState().showMessageInTextView(message);
                        });
                    }

                    @Override
                    public void onSetMaxPosition(int max) {
                        activity.runOnUiThread(() ->
                                getViewState().setMaxPosition(max));
                    }

                    @Override
                    public void onUpdatePosition(ImportFileService.NumericString ns) {
                        activity.runOnUiThread(() ->
                                getViewState().updateProgress(ns));
                    }

                    @Override
                    public void onMessageInTextView(String message) {
                        activity.runOnUiThread(() -> {
                            getViewState().showMessageInTextView(message);
                        });
                    }

                    @Override
                    public void onInfoMessage(String message) {
                        activity.runOnUiThread(() -> {
                            getViewState().showMessage("Done!");
                        });
                    }
                });
                importFileService.importCSV2(file);
                bound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
        MyApplication.INSTANCE.startService(intent);
        MyApplication.INSTANCE.bindService(intent, sc, 0);
    }


    public void unbindService() {
        MyApplication.INSTANCE.unbindService(sc);
    }
}
