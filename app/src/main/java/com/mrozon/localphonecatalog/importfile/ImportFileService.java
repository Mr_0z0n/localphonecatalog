package com.mrozon.localphonecatalog.importfile;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Phone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class ImportFileService extends Service {

    MyBinder binder = new MyBinder();

    public interface ImportFileServiceListener {
        void onChangeVisibleButton(boolean isVisible);
        void onError(String message);
        void onSetMaxPosition(int max);
        void onUpdatePosition(NumericString ns);
        void onMessageInTextView(String message);
        void onInfoMessage(String message);
    }

    private ImportFileServiceListener listener;
    private static Thread thread;
    private Boolean interruptProccess = false;

    public ImportFileServiceListener getListener() {
        return listener;
    }

    public void setListener(ImportFileServiceListener listener) {
        this.listener = listener;
    }

    public ImportFileService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void stopImport(){
        if(thread!=null)
            if(thread.isAlive()) {
                interruptProccess = true;
            }
    }

    public void importCSV2(File file) {
        if(listener==null)
            return;
        interruptProccess = false;
        Context context = MyApplication.INSTANCE.getApplicationContext();
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);
        listener.onChangeVisibleButton(true);
        if (thread==null) {
            thread = new Thread(() -> {
                BufferedReader br = null;
                //count rows
                try {
                    int count = 0;
                    br = new BufferedReader(new FileReader(file));
                    while (br.readLine() != null) {
                        count++;
                    }
                    listener.onSetMaxPosition(count);
                } catch (Exception e) {
                    listener.onError(e.getMessage());
                    listener.onChangeVisibleButton(false);
                    return;
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            listener.onError(e.getMessage());
                            listener.onChangeVisibleButton(false);
                        }
                    }
                }
                //iterable rows
                try {
                    int count = 0;
                    br = new BufferedReader(new FileReader(file));
                    String line;
                    while ((line = br.readLine()) != null) {
                        if(interruptProccess)
                            break;
                        count++;
                        //show progress
                        NumericString ns = new NumericString(line, count);
                        listener.onUpdatePosition(ns);
                        String[] lines = ns.stroka.split(";");
                        if (!lines[0].startsWith("+7") && !lines[0].startsWith("8"))
                            continue;
                        Phone current_phone = new Phone(0, lines[0].toLowerCase(), lines[1].toLowerCase(), lines.length >= 3 ? lines[2].toLowerCase() : "", "");
                        //find already exists phone
                        Phone exist_phone = db.phoneCatalogStore().selectPhoneByNumber(current_phone.number);
                        if (exist_phone == null) {
                            //add new phone
                            db.phoneCatalogStore().insertPhone(current_phone);
                            listener.onMessageInTextView(current_phone.number + " added");
                        } else {
                            //already exists
                            Phone updated = needUpdate(current_phone, exist_phone);
                            String message = current_phone.number + " missed";
                            if (updated != null) {
                                db.phoneCatalogStore().updatePhone(updated);
//                                message = current_phone.number + " update";
                            }
                            listener.onMessageInTextView(message);
                        }
                    }
                } catch (Exception e) {
                    listener.onError("ERROR: " + e.getMessage());
                    return;
                } finally {
                    if (br != null) {
                        try {
                            br.close();
                        } catch (IOException e) {
                            listener.onError("ERROR: " + e.getMessage());
                        }
                    }
                }

                listener.onInfoMessage("Done!");
                thread=null;
            });
            thread.start();
        }
    }

    private Phone needUpdate(Phone new_phone, Phone current_phone){
        boolean overwrite = false;

        String name = current_phone.name;
        String address = current_phone.address;
        String note = current_phone.note;
        if(name.contains(new_phone.name.toLowerCase()))
        {
            //name already exists - add address
            if(!address.contains(new_phone.address)) {
                address += System.lineSeparator()+new_phone.address;
                overwrite = true;
            }
        }
        else
        {
            //name does not exists - add name and address
            overwrite = true;
            name += System.lineSeparator()+new_phone.name;
            if(!address.contains(new_phone.address))
                address += System.lineSeparator()+new_phone.address;
        }
        if(!note.equalsIgnoreCase(new_phone.note)) {
            overwrite = true;
            note = new_phone.note;
        }
        if(overwrite) {
            return new Phone(current_phone.id, current_phone.number, name, address, note);
        }
        return null;
    }


    class MyBinder extends Binder {
        ImportFileService getService() {
            return ImportFileService.this;
        }
    }

    class NumericString{
        private String stroka;
        private int position;

        NumericString(String stroka, int position) {
            this.stroka = stroka;
            this.position = position;
        }

        public String getStroka() {
            return stroka;
        }

        public int getPosition() {
            return position;
        }
    }
}
