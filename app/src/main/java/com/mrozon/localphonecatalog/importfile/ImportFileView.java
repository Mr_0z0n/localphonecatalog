package com.mrozon.localphonecatalog.importfile;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface ImportFileView extends MvpView {
    void showProgress();
    void hideProgress();
    void showMessage(String message);
    void showMessageInTextView(String message);
    void setMaxPosition(int count);
    void updateProgress(ImportFileService.NumericString numericString);
    void visibleButton(boolean isVisible);
}
