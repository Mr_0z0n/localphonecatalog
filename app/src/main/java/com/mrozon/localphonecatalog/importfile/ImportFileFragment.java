package com.mrozon.localphonecatalog.importfile;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.editphone.EditPhonePresenter;
import com.mrozon.localphonecatalog.editphone.EditPhoneView;

import java.io.File;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ImportFileFragment extends MvpAppCompatFragment
        implements ImportFileView {

    public static final String TAG = ImportFileFragment.class.getSimpleName();

    @BindView(R.id.tvImportFile)
    TextView tvImportFile;

    @BindView(R.id.progressBar2)
    ProgressBar progressBar;

    @BindView(R.id.ibStopImport)
    ImageButton ibStopImport;

    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @InjectPresenter
    ImportFilePresenter presenter;

    public static ImportFileFragment newInstance(String file) {
        Bundle args = new Bundle();
        args.putSerializable("file", file);
        ImportFileFragment fragment = new ImportFileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void setTitle(int res){
        if(getActivity()!=null)
            getActivity().setTitle(res);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_import_file, container, false);
        ButterKnife.bind(this, view);

        setTitle(R.string.import_file);
        ibStopImport.setVisibility(View.INVISIBLE);
        tvImportFile.setText("");

        if(getArguments()!=null && !getArguments().isEmpty()) {
            String fname = getArguments().getString("file","");
            File file = new File(fname);
            tvImportFile.setText(file.getName());
            presenter.importCSVAsService(getActivity(),file);
        }

        ibStopImport.setOnClickListener(v->{
            presenter.stopImport();
        });

        return view;
    }

    @Override
    public void onDestroy() {
        presenter.unbindService();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }

    @Override
    public void showMessageInTextView(String message) {
//        Log.d(TAG, "showMessageInTextView: "+message);
        tvMessage.setText(message);
    }

    @Override
    public void setMaxPosition(int count) {
            progressBar.setMax(count);
            tvMessage.setText(getString(R.string.scan_import_file_completed,count));
//            presenter.loadIntoDb();
    }

    @Override
    public void updateProgress(ImportFileService.NumericString numericString) {
        int position = numericString.getPosition();
        if(position<=progressBar.getMax())
            progressBar.setProgress(position);
    }

    @Override
    public void visibleButton(boolean isVisible) {
        ibStopImport.setVisibility(isVisible?View.VISIBLE:View.INVISIBLE);
    }
}
