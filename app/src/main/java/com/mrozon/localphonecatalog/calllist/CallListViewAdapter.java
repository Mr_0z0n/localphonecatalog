package com.mrozon.localphonecatalog.calllist;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Call;
import com.mrozon.localphonecatalog.utils.AppConstants;
import com.mrozon.localphonecatalog.utils.rvpattern.RVAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.CallLog.Calls.INCOMING_TYPE;
import static android.provider.CallLog.Calls.MISSED_TYPE;
import static android.provider.CallLog.Calls.OUTGOING_TYPE;

public class CallListViewAdapter extends RVAdapter {

    private  ItemClickListener itemClickListener = null;
    private Context context;

    public interface ItemClickListener {
        void onItemClick(Call call);
        void onItemLongClick(Call call);
    }

    public CallListViewAdapter(Context context, ItemClickListener itemClickListener) {
        super();
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CallListViewHolder(inflateByViewType(context,viewType,parent));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CallListViewHolder){
            Call call = (Call) items.get(position);
            CallListViewHolder catalogViewHolder = (CallListViewHolder) holder;
            catalogViewHolder.tvName.setText(call.getPhone()==null?"":call.getPhone().name);
            catalogViewHolder.tvCalDate.setText(getTimeStamp(call.getCallDate()));
            catalogViewHolder.tvCallNumber.setText(call.getCallNumber());
            switch (call.getCallType()){
                case INCOMING_TYPE:
                    catalogViewHolder.ivCallType.setImageResource(R.drawable.ic_call_received_black_24dp);
                    break;
                case OUTGOING_TYPE:
                    catalogViewHolder.ivCallType.setImageResource(R.drawable.ic_call_made_black_24dp);
                    break;
                case MISSED_TYPE:
                    catalogViewHolder.ivCallType.setImageResource(R.drawable.ic_call_missed_black_24dp);
                    break;
                default:
                    catalogViewHolder.ivCallType.setImageResource(R.drawable.ic_call_black_24dp);
                    break;
            }
            catalogViewHolder.cvCall.setOnLongClickListener(v -> {
                if(itemClickListener!=null)
                    itemClickListener.onItemClick(call);
                return true;
            });
            catalogViewHolder.cvCall.setOnLongClickListener(v -> {
                if(itemClickListener!=null)
                    itemClickListener.onItemLongClick(call);
                return false;
            });
        }
    }

    private String getTimeStamp(Date date) {
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.getDefault()).format(date);
    }
}
