package com.mrozon.localphonecatalog.calllist;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mrozon.localphonecatalog.MyApplication;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.Screens;
import com.mrozon.localphonecatalog.db.PhoneCatalogDatabase;
import com.mrozon.localphonecatalog.db.model.Call;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class CallListPresenter extends MvpPresenter<CallListView> {
    private Router router;

    public CallListPresenter() {
        router = MyApplication.INSTANCE.getRouter();
    }

    void getItems() {
        Context context = MyApplication.INSTANCE;
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);

        Single<List<Call>> getCallListObservable = Single.create(emitter -> {
                                        try {
                                            List<Call> calls =  readAllCalls();
                                            emitter.onSuccess(calls);
                                        } catch (Exception e) {
                                            emitter.onError(e);
                                        }
                                    });

        Disposable disposable = getCallListObservable.toObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> getViewState().showProgress())
                .subscribe((calls) -> {
                            getViewState().hideProgress();
                            getViewState().updateList(calls);
                            getViewState().showMessage(context.getString(R.string.ok));
                        },
                        (throwable -> {
                            getViewState().hideProgress();
                            getViewState().showMessage(throwable.getMessage());
                        }));
    }

    private List<Call> readAllCalls() {
        List<Call> calls = new ArrayList<>();

        Context context = MyApplication.INSTANCE;
        PhoneCatalogDatabase db = PhoneCatalogDatabase.get(context);

        //TODO change 1000 - count records for call items OR remove ' LIMIT 1000'
        @SuppressLint("MissingPermission")
        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, null, null, CallLog.Calls.DATE + " DESC LIMIT 1000");
        if(cursor==null)
            return calls;
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        while (cursor.moveToNext()) {
            String phNumber = cursor.getString(number);
            String callType = cursor.getString(type);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = cursor.getString(duration);
            int dircode = Integer.parseInt(callType);
            //get phone info in our db
            Phone phone = db.phoneCatalogStore().selectPhoneByNumber(phNumber);

            Call call = new Call(phNumber,dircode,callDayTime,callDuration,phone);
            calls.add(call);
        }
        cursor.close();
        return calls;
    }

    public void showPhone(Phone phone) {
        router.navigateTo(new Screens.ViewPhone(phone));
    }
}
