package com.mrozon.localphonecatalog.calllist;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mrozon.localphonecatalog.db.model.Call;
import com.mrozon.localphonecatalog.db.model.Phone;

import java.util.List;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface CallListView extends MvpView {
    void updateList(List<Call> list);
    void showMessage(String message);
    void showProgress();
    void hideProgress();
}
