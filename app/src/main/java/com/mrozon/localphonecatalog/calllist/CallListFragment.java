package com.mrozon.localphonecatalog.calllist;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.mrozon.localphonecatalog.R;
import com.mrozon.localphonecatalog.db.model.Call;
import com.mrozon.localphonecatalog.db.model.Phone;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogPresenter;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogView;
import com.mrozon.localphonecatalog.phonecatalog.PhoneCatalogViewAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CallListFragment extends MvpAppCompatFragment
        implements CallListView {

    public static final String TAG = CallListFragment.class.getSimpleName();

    @BindView(R.id.rvCallItems)
    RecyclerView rvItems;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectPresenter
    public CallListPresenter presenter;

    private CallListViewAdapter adapter;

    public static CallListFragment newInstance() {
        Bundle args = new Bundle();
        CallListFragment fragment = new CallListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_call_list, container, false);
        ButterKnife.bind(this, view);

        if(getActivity()!=null)
            getActivity().setTitle(R.string.nav_call_list);

        adapter = new CallListViewAdapter(getContext(), new CallListViewAdapter.ItemClickListener() {

            @Override
            public void onItemClick(Call call) {

            }

            @Override
            public void onItemLongClick(Call call) {

            }
        });

        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));
        rvItems.setAdapter(adapter);

        presenter.getItems();

        return view;
    }


    @Override
    public void updateList(List<Call> list) {
        adapter.clearAll();
        for (Call item: list) {
            adapter.add(item);
        }
    }

    @Override
    public void showMessage(String message) {
        if(getView()!=null)
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                    .show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
}
