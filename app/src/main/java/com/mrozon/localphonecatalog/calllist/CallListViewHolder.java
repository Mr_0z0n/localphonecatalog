package com.mrozon.localphonecatalog.calllist;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrozon.localphonecatalog.R;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

class CallListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvCalDate)
    TextView  tvCalDate;

    @BindView(R.id.tvCallNumber)
    TextView  tvCallNumber;

    @BindView(R.id.cvCall)
    CardView cvCall;

    @BindView(R.id.ivCallType)
    ImageView ivCallType;

    CallListViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}
